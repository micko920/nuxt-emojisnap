import { mount } from '@vue/test-utils'
import Instructions from '@/components/Instructions.vue'

describe('Instructions', () => {
  test('is a Vue instance', () => {
    const wrapper = mount(Instructions)

    expect(wrapper.isVueInstance()).toBeTruthy()
  })
})
