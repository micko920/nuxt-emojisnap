module.exports = {
  root: true,
  env: {
    browser: true,
    node: true
  },
  parserOptions: {
    parser: 'babel-eslint'
  },
  extends: [
    'stylelint',
    'prettier/vue',
    'eslint:recommended',
    'plugin:prettier/recommended',
    'plugin:vue/strongly-recommended',
    '@nuxtjs',
    'plugin:nuxt/recommended'
  ],
  plugins: ['nuxt', 'prettier', 'vue'],
  // add your custom rules here
  rules: {
    'vue/component-name-in-template-casing': ['error', 'PascalCase'],
    'space-before-function-paren': 'off',
    'node/no-unsupported-features/es-syntax': 'off',
    'vue/max-attributes-per-line': [
      2,
      {
        singleline: 20,
        multiline: {
          max: 1,
          allowFirstLine: false
        }
      }
    ],
    'vue/html-closing-bracket-newline': 'off',
    'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'vue/html-self-closing': [
      'error',
      {
        html: {
          void: 'always',
          normal: 'always',
          component: 'always'
        },
        svg: 'always',
        math: 'always'
      }
    ]
  },
  globals: {
    $nuxt: true
  }
}
